//
//  AppDelegate.swift
//  Flickr
//
//  Created by Liam Nichols on 10/11/2015.
//  Copyright © 2015 Liam Nichols. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}

